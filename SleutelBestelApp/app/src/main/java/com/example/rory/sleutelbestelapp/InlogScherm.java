package com.example.rory.sleutelbestelapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Rory on 9-4-2015.
 */
public class InlogScherm extends Activity implements View.OnClickListener
{
    public static String ip;

    Button button_ip;
    EditText ipadres;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inlog);
        ipadres = (EditText) findViewById(R.id.ipadres);
        button_ip = (Button)findViewById(R.id.button_ip);
        button_ip.setOnClickListener(this);
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.button_ip:
                ip = ipadres.getText().toString();
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                break;
        }
    }
}
