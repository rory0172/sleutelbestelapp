package com.example.rory.sleutelbestelapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by Rory on 9-4-2015.
 */
public class LaadScherm extends Activity {


    private static int SPLASH_TIME_OUT = 3500;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laad);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent i = new Intent(LaadScherm.this, InlogScherm.class);

                startActivity(i);

                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
